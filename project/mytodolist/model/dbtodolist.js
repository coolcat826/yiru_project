/*
Change Description                                 Change Date             Change By
-------------------------------                    ------------            --------------
Init Version                                       2022/04/06               Yiru Shi
*/
var mysql = require('mysql');
var conf = require('../dbconf');
var basefn = require('../support/basefn');
var request = require('request');
var dbbase = require('../model/dbbase');

var dbconn = mysql.createConnection(conf.db);
var sql = '';

module.exports = {
    //查詢所有資料
    Gettodolistdb: function (req, callback) {
        console.log("model/dbtodolist.js/Gettodolistdb::START");
        table_name = 'todolist',
            dataIn = {
                del: 0
            }
        
console.log("model/dbtodolist.js/Gettodolistdb::CALLFN::dbbase.get_datas");
        dbbase.get_datas(table_name, dataIn, function (err, dataOut) {
console.log("model/dbtodolist.js/Gettodolistdb::CALLBACK::dbbase.get_datas::ERROR = " + JSON.stringify(err) + " & dataOut=" + JSON.stringify(dataOut));
console.log("model/dbdepartment/Gettodolistdb::CALLFN::dbbase.get_datas_count START");
            callback(err, dataOut);
        });//get_datas
    },

    Inserttodolistdb: function (req, callback) {
console.log("model/dbtodolist.js/Inserttodolistdb::START");
console.log("model/dbtodolist.js/Inserttodolistdb::req=" + JSON.stringify(req.body));
        table_spec = {
            table_name: 'todolist',
        }

        dataIn = {
            myevent: req.body.myevent,
            priority: req.body.priority,
        }

console.log("model/dbtodolist.js/Inserttodolistdb::CALLFN::dbbase.InsertData");
        dbbase.InsertData(table_spec, dataIn, function (err, dataOut) {
console.log("model/dbtodolist.js/Inserttodolistdb::CALLBACK::dbbase.get_datas::ERROR = " + JSON.stringify(err) + " & dataOut=" + JSON.stringify(dataOut));
            callback(err, dataOut);
        });
console.log("model/dbtodolist.js/Inserttodolistdb::END");
    },
    Updatetodolist: function (req, callback) {
console.log("model/dbtodolist.js/Updatetodolist::START");
            myevent = (req.body.myevent ? req.body.myevent : ""),
            priority = (req.body.priority ? req.body.priority : ""),
            del = (req.body.del ? req.body.del : ""),

            table_spec = {
                table_name: 'todolist',
            }

        wheredata = {
            pk: req.body.pk
        }

        if (myevent!=="") {
            dataIn = {
                myevent: req.body.myevent,
            }
        }
        if (priority!=="") {
            dataIn = {
                priority: req.body.priority,
            }
        }
        if (del!==""){
            dataIn = {
                del: req.body.del,
            }
        }

console.log("model/dbtodolist.js/Updatetodolist::CALLFN::dbbase.UpDateData");
        dbbase.UpDateData(table_spec, dataIn, wheredata, function (err, dataOut) {
console.log("model/dbtodolist.js/Updatetodolist::CALLBACK::dbbase.UpDateData::ERROR = " + JSON.stringify(err) + " & dataOut=" + JSON.stringify(dataOut));
console.log("model/dbtodolist.js/Updatetodolist::END");
            callback(err, dataOut);
        });
    },
    Getusersearch: function (req, callback) {
console.log("model/dbtodolist.js/Getusersearch::START");
        myevent = (req.body.myevent ? req.body.myevent : ""),
            priority = (req.body.priority ? req.body.priority : ""),
            table_spec = {
                table_name: 'todolist',
            }

        if (myevent) {
            dataIn = {
                myevent: req.body.myevent,
            }
        } else if (priority) {
            dataIn = {
                priority: req.body.priority,
            }
        }
console.log("model/dbtodolist.js/Getusersearch::CALLFN::dbbase.get_like_and");
        dbbase.get_like_and(table_spec, dataIn, function (err, dataOut) {
console.log("model/dbtodolist.js/Getusersearch::CALLBACK::dbbase.get_like_and::ERROR = " + JSON.stringify(err) + " & dataOut=" + JSON.stringify(dataOut));
console.log("model/dbtodolist.js/Getusersearch::END");
            callback(err, dataOut);
        });
    },

}