<?php
//正式
$local_domain = 'http://192.168.50.132:8013';
$frontEnd_domain = 'http://192.168.50.132:8021';
$arkpay_platform = 'arkpay';
$arkpay_pay_platform = 'arkpay_pay';
$arkpay_domain = 'http://61.220.158.181:2228';

//測試
//$local_domain = 'http://192.168.50.132:8213';
//$frontEnd_domain = 'http://192.168.50.132:8221';
//$arkpay_domain = 'http://61.220.158.181:2228';
//$arkpay_platform = 'arkpay_dev';
//$arkpay_pay_platform = 'arkpay_pay_dev';

return [
	//mail api
	'mail_api'=>'https://t1.tracke.site/email/zoho',
	
	//line login取得資訊
	'line_oauth'=>'https://api.line.me/oauth2/v2.1/token',

//arkpay-----------------------------------------start
	'arkpay' => $arkpay_platform,
	'arkpay_pay' => $arkpay_pay_platform,
	//綁定arkpay錢包
	'arkpay_wallet'=>$arkpay_domain.'/api/oauth/authorize',
	'arkpay_wallet_token'=>$arkpay_domain.'/api/business/getToken',
	//提領到arkpay錢包
	'arkpay_withdraw'=>$arkpay_domain.'/api/business/transfer',
	//取得付款url
	'get_pay_arkpay_url'=>$arkpay_domain.'/api/business/urlParamEncrypt',
	//付款頁
	'arkpay_pay_view'=>$arkpay_domain.'/api/pay',
	//取得付款詳細資訊
	'get_arkpay_pay_detail'=>$arkpay_domain.'/api/business/getTxinfoByTaxId',
	//arkpay給出
	'arkpay_re_pay'=>$arkpay_domain.'/api/business/transfer',
//arkpay-----------------------------------------end

	//本地controller網址
	'localhost'=>$local_domain,

	//忘記密碼頁面網址
	'remind'=>$frontEnd_domain.'/pages/reset_pw.html',

	//前端首頁
	'frontEnd_index'=>$frontEnd_domain.'/index.html',

	//使用者前端登入頁面
	'frontEnd_Login'=>$frontEnd_domain.'/pages/login.html',

	//第三方首次登入網址
	'thr_redirect_url'=>$frontEnd_domain.'/pages/setup_step.html',

	// //合約詳細 後面+mert_pk 只有買家可看
	// 'contract_detail'=>'http://192.168.50.132:8221/pages/contract_detail.html?contract_id=',

	//前端網址
	'frontEnd'=>$frontEnd_domain,
];