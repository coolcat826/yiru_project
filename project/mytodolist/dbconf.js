/*
Change Description                             Change Date             Change By
-------------------------------                ------------            --------------
Init Version                                   2022/04/06              Yiru Shi
*/
var mysql = require('mysql');

module.exports = {
    
    db: {
        host: "localhost",
        user: "yiru",
        password: "123456",
        database: "yiru"
    },
};

//資料庫連結
var dbconn = mysql.createConnection(module.exports.db);

dbconn.connect(function (err) {

if (err) {
  console.log('Connecting Error');
  return;
}
  console.log('Connecting Success!');
});