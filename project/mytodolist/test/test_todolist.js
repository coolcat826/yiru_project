const { expect } = require('chai');
const supertest = require('supertest');

const api = supertest('http://localhost:5001'); // 測試 API 路徑

//測試取得所有資料
describe('Todolist_Testing', () => {
    it('Todolist 是否取得所有欄位與資料', (done) => {
        api.get('/todolist') 
        .expect(200)
        .end((err, res) => {
            if (err) {
                done(err);
            }

            expect(res.body.Respdate[0]).to.have.property('myevent');//判斷是否有myevent這個欄位
            expect(res.body.Respdate[0].myevent).to.be.a('string');//判斷是否為字串
            expect(res.body.Respdate[0]).to.have.property('priority');//判斷是否有priority這個欄位
            expect(res.body.Respdate[0].priority).to.be.a('number');//判斷是否為數字
            
            done();
        });
    });

    it('Todolist 新增資料', (done) => {
        api.post('/todolist') // 測試新增資料
        .send({
            myevent: 'test888',
            priority: '3'
        })
        .expect(200,done)
    });

    it('Todolist 修改事項資料', (done) => {
        api.patch('/todolist') // 測試修改資料
        .send({
            myevent: 'test999c',//要修改的欄位與值
            pk:'57' //要修改第幾筆資料
        })
        .expect(200,done)
    });

    it('Todolist 修改優先權資料', (done) => {
        api.patch('/todolist') // 測試修改資料
        .send({
            priority: '2', //將優先權改為2
            pk:'57'//要修改第幾筆資料
        })

        .expect(200,done)
    });

    it('Todolist 刪除資料', (done) => {
        api.patch('/todolist') // 測試刪除資料
        .send({
            del: '1', //刪除將del改成1
            pk:'57'//要刪除第幾筆資料
        })
        .expect(200,done)
    });
    
    it('Todolist 查詢關鍵字取得資料', (done) => {
        api.post('/todolist/usersearch') // 測試關鍵字取得資料
        .send({
            myevent: '123', //要尋找的關鍵字為123
        })
        .expect(200,done)
    });

    it('Todolist 過濾優先權資料', (done) => {
        api.post('/todolist/usersearch') // 測試過濾優先權取得資料
        .send({
            priority: '2', //尋找優先權中等的資料
        })
        .expect(200,done)
    });

})