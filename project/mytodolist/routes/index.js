var express = require('express');
var router = express.Router();

var http = require('http').Server(express);
var io = require('socket.io')(http);

/* GET home page. */

router.get('/testpage', function(req, res, next) {
  res.render('test_page', { title: 'Test  Page!!' });
});

router.get('/testrpc', function(req, res, next) {
  res.render('testrpc', { title: 'Test  Page!!' });
});

module.exports = router;
