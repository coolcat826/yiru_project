/*
Change Description                                 Change Date             Change By
-------------------------------                    ------------            --------------
Init Version                                       2022/04/06              Yiru Shi
*/
var express = require('express');
var router = express.Router();
var request = require('request');

dbtodolist = require('../model/dbtodolist');
var basefn = require('../support/basefn');
var title = "todolist";
router
    .get('/', function (req, res, next) {
console.log("GET::route/todolist.js::START");
console.log("route/todolist.js::reqdate = " + JSON.stringify(req.body));
console.log("route/todolist.js::CALLFN::dbtodolist.gettodolist");
        dbtodolist.Gettodolistdb(req, function (err, results) {
console.log("route/todolist.js::CALLBACK::dbtodolist.gettodolist::ERROR=" + JSON.stringify(err) + " & results=" + JSON.stringify(results));
            Respdate = "";
            Total_count = 0;
            if (err) {
                console.log(err);
                statecode = {
                    scode: '99999',
                    smsg: 'fail',
                    Errormsg: err
                };
            }

            if (results.rowsAffected == 0) {
                statecode = {
                    scode: '00004',
                    smsg: '無資料筆數',
                };
                Total_count = 0;
            } else {
                Respdate = results.RowDataPacket;
                statecode = {
                    scode: '00000',
                    smsg: 'success'
                };
                Total_count = results.rowcount;
                console.log(Respdate);
            }

            dataOut = { title: title, statecode: statecode, Respdate: results }
console.log("route/todolist.js::CALLBACK::dbtodolist.gettodolist::dataOut=" + JSON.stringify(dataOut));
console.log("GET::route/todolist.js::END");
            res.json(dataOut);
        });
    })
    .post('/', function (req, res, next) {
console.log("POST::route/todolist.js::START");
console.log("route/todolist.js::reqdate = " + JSON.stringify(req.body));
console.log("route/todolist.js::CALLFN::dbtodolist.InsertSampledb");
        dbtodolist.Inserttodolistdb(req, function (err, results) {
            console.log("route/todolist.js::CALLBACK::dbtodolist.InsertSampledb::ERR=" + err + "result=" + results);

            if (err) {
                console.log(err);
                statecode = {
                    scode: '00003',
                    smsg: '新增失敗',
                    Errormsg: err,
                };
            } else if (results.affectedRows == 1) {
                statecode = {
                    scode: '00002',
                    smsg: '新增資料成功'
                };
            }

            dataOut = { "title": title, statecode: statecode };

console.log("route/todolist.js::CALLBACK::dbtodolist.InsertSampledb::dataOut=" + dataOut);
console.log("POST::route/todolist.js::END");
            res.json(dataOut);
        });
    })
    .patch('/', function (req, res, next) {
        dbtodolist.Updatetodolist(req, function (err, dataOut) {
                console.log(dataOut);
                if(dataOut.affectedRows >= 1 ){ 
                    statecode = {
                        scode: '00006',
                        smsg: '更新資料成功'
                    };   
                    
            }
            dataOut = { "title": title, statecode: statecode};
            res.json(dataOut);
        });
    })
    .post('/usersearch', function (req, res, next) {
console.log("GET::route/todolist.js/usersearch::START");
console.log("route/todolist.js/usersearch::reqdate = " + JSON.stringify(req.body));
console.log("route/todolist.js/usersearch::CALLFN::dbtodolist.getusersearch");
        dbtodolist.Getusersearch(req, function (err, results) {
console.log("route/todolist.js::CALLBACK::dbtodolist.getusersearch::ERROR=" + JSON.stringify(err) + " & results=" + JSON.stringify(results));
            Respdate = "";
            Total_count = 0;
            if (err) {
                console.log(err);
                statecode = {
                    scode: '99999',
                    smsg: 'fail',
                    Errormsg: err
                };
            }

            if (results.rowsAffected == 0) {
                statecode = {
                    scode: '00004',
                    smsg: '無資料筆數',
                };
                Total_count = 0;
            } else {
                Respdate = results.RowDataPacket;
                statecode = {
                    scode: '00000',
                    smsg: 'success'
                };
                Total_count = results.rowcount;
                console.log(Respdate);
            }

            dataOut = { title: title, statecode: statecode, Respdate: results }
console.log("route/todolist.js::CALLBACK::dbtodolist.getusersearch::dataOut=" + JSON.stringify(dataOut));
console.log("GET::route/todolist.js/getusersearch::END");
                    res.json(dataOut);
                });
    })
    .delete('/',function(req,res,next){
        dbtodolist.Updatetodolist(req, function (err, dataOut) {
            console.log(dataOut);
            if(dataOut.affectedRows >= 1 ){ 
                statecode = {
                    scode: '00006',
                    smsg: '刪除資料成功'
                };   
        }
        dataOut = { "title": title, statecode: statecode};
        res.json(dataOut);
     });
    })
    

module.exports = router;