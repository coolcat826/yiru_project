import React, { useState, useEffect } from 'react';
import { Listitem } from './listitem';
import  Addtodo  from './Addtodo';

function App() {
  const url="http://localhost:5001/todolist/";
  const [items, setItems] = useState([]);
  
  useEffect(() => {
    fetch(url)
    .then(res => res.json())
    .then(json => setItems(json.Respdate)
    
    )
    
  }, [url])
 
  return (
    <>
      <Listitem mytodo={items} />
    </>
  );
}

export default App;
